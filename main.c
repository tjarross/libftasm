/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: smilynux <>                                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/14 00:32:13 by smilynux          #+#    #+#             */
/*   Updated: 2017/09/07 18:04:34 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libftASM.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

FILE	*f;

void	test_strcpy()
{
	char str[128];
	char ft_str[128];

	strcpy(str, "hello world");
	strcpy(ft_str, "hello world");
	if (strcmp(ft_str, str) != 0)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strcpy(ft_str, \"hello world\")");
	strcpy(str, "");
	strcpy(ft_str, "");
	if (strcmp(ft_str, str) != 0)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strcpy(ft_str, \"\")");
	strcpy(str, "hello\0 world");
	strcpy(ft_str, "hello\0 world");
	if (strcmp(ft_str, str) != 0)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strcpy(ft_str, \"hello\\0 world\")");
}

void	test_strcmp()
{
	char str[128];
	char bad_str[128];

	bzero(str, 128);
	bzero(bad_str, 128);
	strcpy(str, "qwertyuiop");
	strcpy(bad_str, "qwertzuiop");
	if (strcmp(bad_str, str) != ft_strcmp(bad_str, str))
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strcmp(\"qwertzuiop\", \"qwertyuiop\")");
	bzero(str, 128);
	bzero(bad_str, 128);
	strcpy(str, "qwert\0uiop");
	strcpy(bad_str, "qwert\0uiop");
	if (strcmp(bad_str, str) != ft_strcmp(bad_str, str))
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strcmp(\"qwert\\0uiop\", \"qwert\\0uiop\")");
	bzero(str, 128);
	bzero(bad_str, 128);
	strcpy(str, "\0qwertyuiop");
	strcpy(bad_str, "\0qwertzuiop");
	if (strcmp(bad_str, str) != ft_strcmp(bad_str, str))
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strcmp(\"\\0qwertzuiop\", \"\\0qwertyuiop\")");
	bzero(str, 128);
	bzero(bad_str, 128);
	strcpy(str, "\0qwertyuiop");
	strcpy(bad_str, "\0qwertzuiop");
	if (strcmp(bad_str, str) != ft_strcmp(bad_str, str))
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strcmp(\"\\0qwertzuiop\", \"\\0qwertyuiop\")");
}

void	test_memcmp()
{
	char str[128];
	char bad_str[128];

	bzero(str, 128);
	bzero(bad_str, 128);
	strcpy(str, "qwertyuiop");
	strcpy(bad_str, "qwertzuiop");
	if (memcmp(bad_str, str, 128) != ft_memcmp(bad_str, str, 128))
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_memcmp(\"qwertzuiop\", \"qwertyuiop\", 128)");
	bzero(str, 128);
	bzero(bad_str, 128);
	strcpy(str, "qwert\0uiop");
	strcpy(bad_str, "qwert\0uiop");
	if (memcmp(bad_str, str, 128) != ft_memcmp(bad_str, str, 128))
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_memcmp(\"qwert\\0uiop\", \"qwert\\0uiop\", 128)");
	bzero(str, 128);
	bzero(bad_str, 128);
	strcpy(str, "\0qwertyuiop");
	strcpy(bad_str, "\0qwertzuiop");
	if (memcmp(bad_str, str, 128) != ft_memcmp(bad_str, str, 128))
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_memcmp(\"\\0qwertzuiop\", \"\\0qwertyuiop\", 128)");
	bzero(str, 128);
	bzero(bad_str, 128);
	strcpy(str, "\0qwertyuiop");
	strcpy(bad_str, "\0qwertzuiop");
	if (memcmp(bad_str, str, 0) != ft_memcmp(bad_str, str, 0))
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_memcmp(\"\\0qwertzuiop\", \"\\0qwertyuiop\", 0)");
}

void	test_strchr()
{
	char str[] = "coucou ca va toi ?";
	if (ft_strchr(str, 't') != strchr(str, 't'))
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strchr(str, 't')");
	if (ft_strchr(str, '\0') != strchr(str, '\0'))
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strchr(str, '\\0')");
	if (ft_strchr(NULL, 't') != NULL)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strchr(NULL, 't')");
}

void	test_memset()
{
	char str[128];
	char ft_str[128];

	memset(str, '1', 128);
	ft_memset(ft_str, '1', 128);
	if (memcmp(ft_str, str, 128) != 0)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_memset(t_str, '1', 128)");
	memset(str, '\0', 64);
	ft_memset(ft_str, '\0', 64);
	if (memcmp(ft_str, str, 64) != 0)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_memset(t_str, '\\0', 64)");
	memset(str, '1', 0);
	ft_memset(ft_str, '1', 0);
	if (memcmp(ft_str, str, 0) != 0)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_memset(t_str, '1', 0)");
	if (ft_memset(NULL, '1', 128) != NULL)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_memset(NULL, '1', 128)");

}

void	test_strdup()
{
	char *str;
	char *ft_str;

	str = strdup("coucou");
	ft_str = ft_strdup("coucou");
	if (strcmp(ft_str, str) != 0)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strdup(\"coucou\"i)");
	free(str);
	free(ft_str);

	str = strdup("");
	ft_str = ft_strdup("");
	if (strcmp(ft_str, str) != 0)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strdup(\"\")");
	free(str);
	free(ft_str);
	if (ft_strdup(NULL) != NULL)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strdup(NULL)");
}

void	test_strcat()
{
	char	*ft_str;
	char	*str;

	str = malloc(23);
	ft_str = malloc(23);
	strcpy(str, "bonjour");
	strcpy(ft_str, "bonjour");
	str = strdup(strcat(str, " comment ca va"));
	ft_str = strdup(ft_strcat(ft_str, " comment ca va"));
	if (strcmp(ft_str, str) != 0)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_strcat(\"bonjour\", \" comment ca va\")");
	free(str);
	free(ft_str);
}

void	test_bzero()
{
	char ft_str[128];
	char str[128];

	memset(ft_str, '1', 128);
	memset(str, '1', 128);
	ft_bzero(ft_str, 128);
	bzero(str, 128);
	if (memcmp(ft_str, str, 128) != 0)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_bzero(ft_str, 128)");

	memset(ft_str, '1', 128);
	memset(str, '1', 128);
	ft_bzero(ft_str, 0);
	bzero(str, 0);
	if (memcmp(ft_str, str, 128) != 0)
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", "ft_bzero(ft_str, 0)");
	ft_bzero(NULL, 6);
}

void	check_memcpy_res(char *str_dest, char *str_src, size_t len)
{
	char str[50];

	if (str_src[3] == 0)
		sprintf(str, "ft_memcpy(\"ghi\\0kl\", \"abc\\0ef\", %zu)", len);
	else
		sprintf(str, "ft_memcpy(\"%s\", \"%s\", %zu)", str_dest, str_src, len);

	char *str_res1 = strdup(memcpy(str_dest, str_src, len));
	char *str_res2 = strdup(ft_memcpy(str_dest, str_src, len));
	if (memcmp(str_res1, str_res2, len))
		fprintf(f, "%s \033[0;31mFAIL\033[0;29m\n", str);
	free(str_res1);
	free(str_res2);
	free(str_src);
	free(str_dest);
}

void	test_memcpy()
{
	check_memcpy_res(strdup("abcdef"), strdup("ghijkl"), 6);
	check_memcpy_res(strdup("abcdef"), strdup("ghijkl"), 3);
	check_memcpy_res(strdup("abcdef"), strdup("ghijkl"), 0);
	check_memcpy_res(strdup("abc\0ef"), strdup("ghi\0kl"), 6);
	if (ft_memcpy(NULL, "abcdef", 6) != NULL)
		fprintf(f, "ft_memcpy(NULL, \"abcdef\", 6) \033[0;31mFAIL\033[0;29m\n");
	if (strcmp(ft_memcpy("ghijkl", NULL, 6), "ghijkl"))
		fprintf(f, "ft_memcpy(\"ghijkl\", NULL, 6) \033[0;31mFAIL\033[0;29m\n");
	if (ft_memcpy(NULL, NULL, 6) != NULL)
		fprintf(f, "ft_memcpy(NULL, NULL, 6) \033[0;31mFAIL\033[0;29m\n");
}

void	test_strlen()
{
	if (strlen("basic_string") != ft_strlen("basic_string"))
		fprintf(f, "ft_strlen(\"basic_string\") \033[0;31mFAIL\033[0;29m\n");
	if (strlen("l") != ft_strlen("l"))
		fprintf(f, "ft_strlen(\"l\") \033[0;31mFAIL\033[0;29m\n");
	if (strlen("") != ft_strlen(""))
		fprintf(f, "ft_strlen(\"\") \033[0;31mFAIL\033[0;29m\n");
	if (ft_strlen(NULL) != 0)
		fprintf(f, "ft_strlen(NULL) \033[0;31mFAIL\033[0;29m\n");
}

void	test_is()
{
	for (unsigned int i = 0; i < 256; ++i)
	{
		if (ft_isalpha(i) != (isalpha(i) != 0))
			fprintf(f, "ft_isalpha(%d) \033[0;31mFAIL\033[0;29m\n", i);
		if (ft_isalnum(i) != (isalnum(i) != 0))
			fprintf(f, "ft_isalnum(%d) \033[0;31mFAIL\033[0;29m\n", i);
		if (ft_isdigit(i) != (isdigit(i) != 0))
			fprintf(f, "ft_isdigit(%d) \033[0;31mFAIL\033[0;29m\n", i);
		if (ft_isascii(i) != (isascii(i) != 0))
			fprintf(f, "ft_isascii(%d) \033[0;31mFAIL\033[0;29m\n", i);
		if (ft_isprint(i) != (isprint(i) != 0))
			fprintf(f, "ft_isprint(%d) \033[0;31mFAIL\033[0;29m\n", i);
		if (ft_toupper(i) != toupper(i))
			fprintf(f, "ft_toupper(%d) \033[0;31mFAIL\033[0;29m\n", i);
		if (ft_tolower(i) != tolower(i))
			fprintf(f, "ft_tolower(%d) \033[0;31mFAIL\033[0;29m\n", i);
	}
}

int		main(void)
{
	char filename[] = "libasm.log";
	f = fopen(filename, "wr");

	test_bzero();
	test_strcat();
	test_is();
	test_strlen();
	test_memset();
	test_memcpy();
	test_strdup();

	test_strchr();
	test_memcmp();
	test_strcmp();
	test_strcpy();

	fseek(f, 0, SEEK_END);
	fclose(f);
	return (0);
}
