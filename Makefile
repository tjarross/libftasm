# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/04/19 12:34:43 by tjarross          #+#    #+#              #
#    Updated: 2017/09/06 18:06:17 by tjarross         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

OS =		$(shell uname -s)
NAME =		libfts.a
SRC_NAME =	ft_bzero ft_strcat ft_isalpha ft_isdigit ft_isalnum ft_isascii \
			ft_isprint ft_toupper ft_tolower ft_puts ft_strlen ft_memset \
			ft_memcpy ft_strdup ft_cat ft_strcpy ft_strchr ft_putchar \
			ft_putmdr ft_memcmp ft_strcmp
SRC =  		$(addprefix src/, $(SRC_NAME:=.s))
OBJ =		$(SRC:%.s=%.o)
CC =		nasm

ifeq ($(OS), Darwin)
FLAGS =		-f macho64
PREFIX =	--prefix _
else
FLAGS =		-f elf64
PREFIX =
endif

all: $(NAME)

msg:
	@echo "\033[0;29m⌛  Making Lib : \c"

$(NAME): msg $(OBJ)
	@echo ""
	@ar rcs $(NAME) $(OBJ)
	@ranlib $(NAME)
	@echo "\033[0;34m✅  Lib created !\033[0;29m"

src/%.o: src/%.s
	@$(CC) $(FLAGS) -o $@ $(PREFIX) $^
	@echo "\033[0;32m.\c\033[0;29m"

clean:
	@echo "\033[0;31m🔥  Cleaning Lib Objects...\033[0;29m"
	@rm -rf $(OBJ)

fclean: clean clean_test
	@echo "\033[0;31m🔥  Cleaning Library...\033[0;29m"
	@rm -rf $(NAME)

clean_test:
	@rm -rf test
	@rm -rf main.o

re: fclean all

test:
	@make
	@gcc -c main.c -I ./include
	@gcc -o test main.o -L. -lfts

.PHONY: all clean fclean re msg test clean_test
