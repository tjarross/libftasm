section .text

global ft_tolower

ft_tolower:
	cmp rdi, 0x41
	jl _false
	cmp rdi, 0x5a
	jle _true
	mov rax, rdi
	ret

_false:
	mov rax, rdi
	ret

_true:
	add rdi, 32
	mov rax, rdi
	ret
