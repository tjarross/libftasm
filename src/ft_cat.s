%ifndef __LINUX__
%define SYS_READ 0x2000003
%define SYS_WRITE 0x2000004
%else
%define SYS_READ 0
%define SYS_WRITE 1
%endif

section .text

global	ft_cat

extern	malloc
extern	free

ft_cat:
	push	rdi
	mov		rdi, 4096
	call	malloc
	mov		r9, rax
	pop		rdi
	bt		rdi, 31
	jc		_failure
	push	rax
	jmp		_cat_loop
	ret

_cat_loop:
	mov		rsi, r9
	mov		rdx, 4096
	mov		rax, SYS_READ
	syscall
	jc		_failure
	mov		rdx, rax
	cmp		rax, 0
	je		_success
	mov		r8, rdi
	mov		rdi, 1
	mov		rsi, r9
	mov		rax, SYS_WRITE
	syscall
	jc		_failure
	mov		rdi, r8
	jmp		_cat_loop

_failure:
	pop		rdi
	call	free
	mov		rax, 1
	ret

_success:
	pop		rdi
	call	free
	mov		rax, 0
	ret
