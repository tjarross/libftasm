section .text

global	ft_strchr

ft_strchr:
	cmp	rdi, 0
	je	_failure
	jmp	_loop
	ret

_loop:
	mov	al, byte[rdi]
	cmp	sil, al
	je	_success
	cmp	al, 0
	je	_failure
	inc	rdi
	jmp _loop

_failure:
	mov	rax, 0
	ret

_success:
	mov rax, rdi
	ret
