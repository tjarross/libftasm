section .text

extern malloc
extern ft_strlen
extern ft_memcpy

global ft_strdup

ft_strdup:
	cmp		rdi, 0
	je		_failure
	push	rbp
	mov		rbp, rsp
	push	rdi
	call	ft_strlen
	mov		rdi, rax
	inc		rdi
	push	rdi
	call	malloc
	pop		rdx
	pop		rsi
	mov		rdi, rax
	call	ft_memcpy
	pop		rbp
	ret

_failure:
	mov		rax, 0
	ret
