section .text

global ft_memset

extern	memset

ft_memset:
	cmp		rdi, 0
	je		_failure
	push	rdi
	mov		rcx, rdx
	mov		rax, rsi
	cld
	rep		stosb
	pop		rax
	ret

_failure:
	mov		rax, 0
	ret
