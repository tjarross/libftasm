section .text

extern ft_putchar

global ft_putmdr

ft_putmdr:
	mov		rdi, 'm'
	call	ft_putchar
	mov		rdi, 'd'
	call	ft_putchar
	mov		rdi, 'r'
	call	ft_putchar
	mov		rdi, 0xa
	call	ft_putchar
	ret
