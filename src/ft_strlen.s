section .text

global ft_strlen

ft_strlen:
	cmp		rdi, 0
	je		_null
	push	rdi
	mov		al, 0x00
	mov		rcx, -1
	cld
	repnz	scasb
	add		rcx, 1
	not		rcx
	mov		rax, rcx
	pop		rdi
	ret

_null:
	mov		rax, 0
	ret
