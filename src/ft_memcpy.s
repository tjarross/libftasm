section .text

global ft_memcpy

ft_memcpy:
	cmp		rdi, 0
	je		_failure
	cmp		rsi, 0
	je		_failure
	push	rdi
	mov		rcx, rdx
	cld
	rep		movsb
	pop		rax
	ret

_failure:
	mov		rax, rdi
	ret
