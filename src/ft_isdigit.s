section .text

global ft_isdigit

ft_isdigit:
	cmp	rdi, 0x30
	jl	_false
	cmp	rdi, 0x39
	jle	_true
	mov rax, 0x0
	ret

_false:
	mov rax, 0x0
	ret

_true:
	mov rax, 0x1
	ret
