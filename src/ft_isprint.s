section .text

global ft_isprint

ft_isprint:
	cmp	rdi, 0x20
	jl	_false
	cmp	rdi, 0x7e
	jle	_true
	mov rax, 0x0
	ret

_false:
	mov rax, 0x0
	ret

_true:
	mov rax, 0x1
	ret
