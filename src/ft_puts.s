%ifndef __LINUX__
%define SYS_WRITE 0x2000004
%else
%define SYS_WRITE 1
%endif

section .text

global ft_puts
extern ft_strlen

ft_puts:
	cmp		rdi, 0
	je		_exit
	push	rdi
	call	ft_strlen
	pop		rsi
	mov		rdx, rax
	mov		rdi, 1
	mov		rax, SYS_WRITE
	syscall
	mov		rsi, 0xa
	push	rsi
	mov		rdi, 1
	mov		rsi, rsp
	mov		rdx, 1
	mov		rax, SYS_WRITE
	syscall
	pop		rsi
	ret

_exit:
	ret
