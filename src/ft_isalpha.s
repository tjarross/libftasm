section .text

global ft_isalpha

ft_isalpha:
	cmp	rdi, 0x41
	jl	_false
	cmp	rdi, 0x5a
	jle	_true
	cmp rdi, 0x61
	jl _false
	cmp rdi, 0x7a
	jle _true
	mov rax, 0x0
	ret

_false:
	mov rax, 0x0
	ret

_true:
	mov rax, 0x1
	ret
