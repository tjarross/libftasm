section .text

extern ft_memset

global ft_bzero

ft_bzero:
	mov rdx, rsi
	mov rsi, 0x00
	call ft_memset
	ret
