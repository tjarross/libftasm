%ifndef __LINUX__
%define SYS_WRITE 0x2000004
%else
%define SYS_WRITE 1
%endif
section .text

global	ft_putchar

ft_putchar:
	push	rdi
	mov		rsi, rsp
	mov		rdi, 1
	mov		rdx, 1
	mov		rax, SYS_WRITE
	syscall
	pop		rdi
	ret
