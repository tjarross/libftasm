section .text

global ft_strcpy

extern	ft_strlen

ft_strcpy:
	push	rdi
	push	rdi
	mov		rdi, rsi
	call	ft_strlen
	pop		rdi
	mov		rcx, rax
	cld
	rep		movsb
	pop		rax
	ret
