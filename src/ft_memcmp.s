section .text

global	ft_memcmp

ft_memcmp:
	xor	rax, rax
	xor rcx, rcx
	jmp _loop

_loop:
	cmp		rcx, rdx
	je		_finish_0
	mov		al, byte[rdi+rcx]
	cmp		al, byte[rsi+rcx]
	jne		_finish
	inc		rcx
	jmp		_loop

_finish_0:
	xor		rax, rax
	ret

_finish:
	movzx	rax, al
	movzx	rdx, byte[rsi+rcx]
	sub		rax, rdx
	ret
