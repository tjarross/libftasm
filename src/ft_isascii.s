section .text

global ft_isascii

ft_isascii:
	cmp	rdi, 0x0
	jl	_false
	cmp	rdi, 0x7f
	jle	_true
	mov rax, 0x0
	ret

_false:
	mov rax, 0x0
	ret

_true:
	mov rax, 0x1
	ret
