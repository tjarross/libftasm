section .text

global ft_strcat

extern ft_strlen
extern ft_memcpy

ft_strcat:
	cmp		rdi, 0
	je		_null
	cmp		rsi, 0
	je		_null
	call	ft_strlen
	mov		rdx, rax
	push	rdi
	add		rdi, rdx
	push	rdi
	mov		rdi, rsi
	push	rsi
	call	ft_strlen
	pop		rsi
	mov		rdx, rax
	inc		rdx
	pop		rdi
	call	ft_memcpy
	pop		rax
	ret

_null:
	mov		rax, rdi
	ret
