section .text

global	ft_strcmp

ft_strcmp:
	push	rdi
	push	rsi
	cld
	jmp		_loop

_loop:
	cmpsb
	jne		_finish
	cmp		byte[rdi], 0
	je		_finish
	cmp		byte[rsi], 0
	je		_finish
	jmp		_loop

_finish:
	xor		rax, rax
	xor		rdx, rdx
	dec		rsi
	dec		rdi
	movzx	rax, byte[rdi]
	movzx	rdx, byte[rsi]
	sub		rax, rdx
	pop		rsi
	pop		rdi
	ret
