section .text

global ft_toupper

ft_toupper:
	cmp rdi, 0x61
	jl _false
	cmp rdi, 0x7a
	jle _true
	mov rax, rdi
	ret

_false:
	mov rax, rdi
	ret

_true:
	sub rdi, 32
	mov rax, rdi
	ret
