/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftASM.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: smilynux <>                                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/14 00:35:30 by smilynux          #+#    #+#             */
/*   Updated: 2017/09/06 19:31:15 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTASM_H
# define LIBFTASM_H

typedef unsigned long size_t;

void	ft_bzero(void *s, size_t n);						//
char	*ft_strcat(char *dest, const char *src);			//
int		ft_isalpha(int c);									//
int		ft_isdigit(int c);									//
int		ft_isalnum(int c);									//
int		ft_isascii(int c);									//
int		ft_isprint(int c);									//
int		ft_toupper(int c);									//
int		ft_tolower(int c);									//
int		ft_puts(const char *s);								//
size_t	ft_strlen(const char *str);							//
void	*ft_memset(void *s, int c, size_t n);				//
void	*ft_memcpy(void *dest, const void *src, size_t n);	//
char	*ft_strdup(const char *s);							//
size_t	ft_cat(int fd);										//

//bonus
char    *ft_strcpy(char *dest, const char *src);			//
void    ft_putmdr();										//
void    ft_putchar(int c);									//
int		ft_memcmp(const char *s1, const char *s2, size_t n);//
int		ft_strcmp(const char *s1, const char *s2);			//
char	*ft_strchr(const char *s, int c);					//

#endif
